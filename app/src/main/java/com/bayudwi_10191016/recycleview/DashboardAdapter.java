package com.bayudwi_10191016.recycleview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardHolder> {

    private ArrayList<SetterGetter> listdata;

    public DashboardAdapter(ArrayList<SetterGetter> listdata) {
        this.listdata = listdata;
    }

    @NonNull
    @Override
    public DashboardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent,false);
        DashboardHolder holder = new DashboardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardHolder holder, int position) {

        final SetterGetter getData = listdata.get(position);
        String judulMenu = getData.getJudul();
        String gambarMenu = getData.getGambar();

        holder.judulMenu.setText(judulMenu);
        if(gambarMenu.equals("logohewan1")) {
            holder.gambarMenu.setImageResource(R.drawable.panda);
        } else if(gambarMenu.equals("logohewan2")) {
            holder.gambarMenu.setImageResource(R.drawable.cow);
        } else if(gambarMenu.equals("logohewan3")) {
            holder.gambarMenu.setImageResource(R.drawable.crocodile);
        } else if(gambarMenu.equals("logohewan4")) {
            holder.gambarMenu.setImageResource(R.drawable.kudanil);
        } else if(gambarMenu.equals("logohewan5")) {
            holder.gambarMenu.setImageResource(R.drawable.fox);
        } else if(gambarMenu.equals("logohewan6")) {
            holder.gambarMenu.setImageResource(R.drawable.giraffe);
        } else if(gambarMenu.equals("logohewan7")) {
            holder.gambarMenu.setImageResource(R.drawable.lion);
        } else if(gambarMenu.equals("logohewan8")) {
            holder.gambarMenu.setImageResource(R.drawable.rabbit);
        } else if(gambarMenu.equals("logohewan9")) {
            holder.gambarMenu.setImageResource(R.drawable.sheep);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashboardHolder extends RecyclerView.ViewHolder {

        TextView judulMenu;
        ImageView gambarMenu;

        public DashboardHolder(@NonNull View itemView) {
            super(itemView);

            judulMenu = itemView.findViewById(R.id.judul_menu);
            gambarMenu = itemView.findViewById(R.id.logo_menu);
        }
    }
}
