package com.bayudwi_10191016.recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datahewan;
    GridLayoutManager gridLayoutManager;
    DashboardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.menuDashboard);

        addData();
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashboardAdapter(datahewan);
        recyclerView.setAdapter(adapter);
    }

    public void addData() {

        datahewan = new ArrayList<>();
        datahewan.add(new SetterGetter("Panda", "logohewan1"));
        datahewan.add(new SetterGetter("Sapi", "logohewan2"));
        datahewan.add(new SetterGetter("Buaya", "logohewan3"));
        datahewan.add(new SetterGetter("Kudanil", "logohewan4"));
        datahewan.add(new SetterGetter("Rubah", "logohewan5"));
        datahewan.add(new SetterGetter("Jerapah", "logohewan6"));
        datahewan.add(new SetterGetter("Singa", "logohewan7"));
        datahewan.add(new SetterGetter("Kelinci", "logohewan8"));
        datahewan.add(new SetterGetter("Domba", "logohewan9"));
    }
}